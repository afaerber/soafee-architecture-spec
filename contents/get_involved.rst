===================
How to Get Involved
===================

For more information please go to: 

* General Information: https://soafee.io 
* Source code repository: https://gitlab.com/soafee 
* Join SOAFEE: https://soafee.io/community/join/ 
* Public calendar: `Web view <https://www.soafee.io/community/calendar>`_, `ICAL <https://calendar.google.com/calendar/ical/c_mi011bbj3kuraljg8k92kk8n84%40group.calendar.google.com/public/basic.ics>`_
